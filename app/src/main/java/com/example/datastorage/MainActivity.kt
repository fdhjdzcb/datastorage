package com.example.datastorage

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.example.compose.DataStorageTheme
import com.example.datastorage.model.Contact
import com.example.datastorage.model.Note
import com.example.datastorage.repository.ContactsRepository
import com.example.datastorage.repository.NotesRepository
import kotlinx.coroutines.launch


@Suppress("DEPRECATION", "NAME_SHADOWING")
class MainActivity : ComponentActivity() {
    private lateinit var contactRepository: ContactsRepository
    private lateinit var noteRepository: NotesRepository

    private var contacts by mutableStateOf(emptyList<Contact>())
    private var notes by mutableStateOf(emptyList<Note>())

    private var selectedContactId by mutableLongStateOf(0L)
    private var isEditDialogVisible by mutableStateOf(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeRepositories()
        importData()
        setContent {
            DataStorageTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen()
                }
            }
        }
        checkAndRequestContactsPermission()
    }

    private fun initializeRepositories() {
        contactRepository = ContactsRepository(this)
        noteRepository = NotesRepository(this)
    }

    private fun importData() {
        importContacts()
        importNotes()
    }

    private var isContactsImported by mutableStateOf(false)
    private var isNotesImported by mutableStateOf(false)

    private fun importContacts() {
        if (!isContactsImported) {
            lifecycleScope.launch {
                contactRepository.importContacts()
                contacts = contactRepository.getAllContacts()
            }
            isContactsImported = true
        }
    }

    private fun importNotes() {
        if (!isNotesImported) {
            lifecycleScope.launch {
                notes = noteRepository.getAllNotes()
            }
            isNotesImported = true
        }
    }

    private val READ_CONTACTS_PERMISSION_REQUEST_CODE = 1001
    private val WRITE_CONTACTS_PERMISSION_REQUEST_CODE = 1002

    private fun checkAndRequestContactsPermission() {
        when (true) {
            !hasReadContactsPermission() -> requestReadContactsPermission()
            !hasWriteContactsPermission() -> requestWriteContactsPermission()
            else -> importContacts()
        }
    }

    private fun hasReadContactsPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestReadContactsPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.READ_CONTACTS),
            READ_CONTACTS_PERMISSION_REQUEST_CODE
        )
    }

    private fun hasWriteContactsPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestWriteContactsPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_CONTACTS),
            WRITE_CONTACTS_PERMISSION_REQUEST_CODE
        )
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            READ_CONTACTS_PERMISSION_REQUEST_CODE,
            WRITE_CONTACTS_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    importContacts()
            }

            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }


    @Composable
    private fun MainScreen() {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .zIndex(1f)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colorScheme.primary),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(R.string.app_title),
                    modifier = Modifier.padding(8.dp),
                    style = MaterialTheme.typography.headlineMedium,
                    fontWeight = FontWeight.Bold,
                    color = Color.White,
                )
            }
        }
        Column {
            LazyColumn(
                modifier = Modifier
                    .weight(1f)
            ) {
                item { Spacer(modifier = Modifier.height(50.dp)) }
                itemsIndexed(contacts) { index, contact ->
                    val note = notes.find { it.contactId == contact.id }
                    val isEven = index % 2 == 0

                    ContactItem(
                        contact = contact,
                        note = note,
                        onEditClick = {
                            selectedContactId = contact.id
                            isEditDialogVisible = true
                        },
                        onDeleteClick = {
                            lifecycleScope.launch {
                                val noteToDelete = noteRepository.getNoteByContactId(contact.id)
                                if (noteToDelete != null) {
                                    noteRepository.deleteNote(noteToDelete)
                                }
                                notes = noteRepository.getAllNotes()
                            }
                        },
                        backgroundColor = if (isEven) Color(0xFFDDDDDD) else Color.White // Устанавливаем цвет фона в зависимости от четности индекса
                    )
                }
            }

            if (isEditDialogVisible) {
                val note = notes.find { note -> note.contactId == selectedContactId }
                EditContactDialog(
                    oldNote = note?.description ?: stringResource(R.string.empty),
                    onDismiss = {
                        isEditDialogVisible = false
                    },
                    onSave = { newNote: String ->
                        lifecycleScope.launch {
                            val idNote = noteRepository.getNoteIdByContactId(selectedContactId)
                            if (idNote != null) {
                                val editedNote = Note(
                                    id = idNote,
                                    description = newNote,
                                    contactId = selectedContactId
                                )
                                noteRepository.updateNote(editedNote)
                            } else {
                                val note = Note(
                                    description = newNote,
                                    contactId = selectedContactId
                                )
                                noteRepository.insertNote(note)
                            }
                            notes = noteRepository.getAllNotes()
                        }
                        isEditDialogVisible = false
                    }
                )
            }
        }
    }


    @Composable
    private fun ContactItem(
        contact: Contact,
        note: Note?,
        onEditClick: () -> Unit,
        onDeleteClick: () -> Unit,
        backgroundColor: Color
    ) {
        val noteText: String = note?.description ?: stringResource(R.string.empty)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(backgroundColor)
                .padding(16.dp)
        ) {
            Text(
                text = contact.name,
                style = MaterialTheme.typography.headlineMedium,
                textAlign = TextAlign.Center
            )
            Text(
                text = "${stringResource(R.string.phone)}: ${contact.phoneNumber}",
                style = MaterialTheme.typography.headlineSmall,
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "${stringResource(R.string.note)}: $noteText",
                    style = MaterialTheme.typography.headlineSmall,
                    color = if (noteText != stringResource(R.string.empty)) Color.Black else Color.Gray,
                    modifier = Modifier.weight(1f)
                )
                ContactActionButton(Icons.Default.Edit) {
                    onEditClick()
                }
                ContactActionButton(Icons.Default.Delete) {
                    onDeleteClick()
                }
            }
        }
    }


    @Composable
    private fun ContactActionButton(icon: ImageVector, onClick: () -> Unit) {
        IconButton(
            onClick = onClick,
            modifier = Modifier.size(40.dp)
        ) {
            Icon(
                imageVector = icon,
                contentDescription = null,
                tint = MaterialTheme.colorScheme.primary
            )
        }
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun EditContactDialog(
        oldNote: String,
        onDismiss: () -> Unit,
        onSave: (newNote: String) -> Unit
    ) {
        var newNote by remember { mutableStateOf(TextFieldValue(oldNote)) }

        AlertDialog(
            onDismissRequest = { onDismiss() },
            title = { Text(stringResource(R.string.edit_note_title)) },
            text = {
                Column {
                    TextField(
                        value = newNote,
                        onValueChange = { newNote = it },
                        label = { Text(stringResource(R.string.new_note_field_title)) }
                    )
                }
            },
            confirmButton = {
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Button(
                        onClick = {
                            onSave(newNote.text)
                            onDismiss()
                        },
                        contentPadding = PaddingValues(10.dp)
                    ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Icon(
                                imageVector = Icons.Default.Add,
                                contentDescription = null,
                                tint = Color.White
                            )
                            Spacer(modifier = Modifier.width(8.dp))
                            Text(stringResource(R.string.save))
                        }
                    }

                    Button(
                        onClick = { onDismiss() },
                        contentPadding = PaddingValues(10.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Clear,
                            contentDescription = null,
                            tint = Color.White
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Text(stringResource(R.string.cancel))
                    }
                }
            }
        )
    }
}