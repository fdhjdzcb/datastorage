package com.example.datastorage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.datastorage.dao.ContactDAO
import com.example.datastorage.dao.NoteDAO
import com.example.datastorage.model.Contact
import com.example.datastorage.model.Note


@Database(entities = [Contact::class, Note::class], version = 1, exportSchema = false)
abstract class DataStorageDatabase : RoomDatabase() {

    abstract fun contactDao(): ContactDAO
    abstract fun noteDao(): NoteDAO

    companion object {
        @Volatile
        private var INSTANCE: DataStorageDatabase? = null

        fun getDatabase(context: Context): DataStorageDatabase {
            return INSTANCE ?: synchronized(this) {
                buildDatabase(context).also { INSTANCE = it }
            }
        }

        private fun buildDatabase(context: Context): DataStorageDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                DataStorageDatabase::class.java,
                "app_database"
            ).build()
        }
    }
}
