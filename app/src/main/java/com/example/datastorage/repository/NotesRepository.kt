package com.example.datastorage.repository

import android.content.Context
import com.example.datastorage.dao.NoteDAO
import com.example.datastorage.database.DataStorageDatabase
import com.example.datastorage.model.Note

class NotesRepository(context: Context) {
    private val noteDao: NoteDAO = DataStorageDatabase.getDatabase(context).noteDao()

    suspend fun getAllNotes(): List<Note> = noteDao.getAllNotes()

    suspend fun insertAll(notes: List<Note>) = noteDao.insertAll(notes)

    suspend fun insertNote(note: Note): Long = noteDao.insertNote(note)

    suspend fun updateNote(note: Note) = noteDao.updateNote(note)

    suspend fun deleteNote(note: Note) = noteDao.deleteNote(note)

    suspend fun getNoteByContactId(contactId: Long): Note? = noteDao.getNoteByContactId(contactId)

    suspend fun getNoteIdByContactId(contactId: Long): Long? = noteDao.getNoteIdByContactId(contactId)
}